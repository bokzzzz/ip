

function setNotification(notification){
	var html="";
	if(notification.includes("app")){
	$.ajax({
        type: 'GET',
        data: {},
        url: 'data/notification',
        dataType: 'json',
        success:function(res){
			if(res.length > 0){
            for(i in res){
				html+="<div >";
				html+="<button onclick='viewNotification("+res[i].id+")' class='btn btn-success notificationButton'>"+res[i].title+"</button>"
				html+="</div>";
			}
			$("#notificationDiv").html(html);
			}else{
				html+="<label>You do not have notifications.";
				$("#notificationDiv").html(html);
			}
		}
        });
	}else{
		html+="<label>You do not choice notification.<label>";
		$("#notificationDiv").html(html);
	}

}
function viewNotification(id){
	const form = document.createElement('form');
  	form.method = 'post';
  	form.action = 'notification';
    const hiddenField = document.createElement('input');
    hiddenField.type = 'hidden';
    hiddenField.name = 'id';
    hiddenField.value = id;
    form.appendChild(hiddenField);
  	document.body.appendChild(form);
  	form.submit();
}
function setMapStat(){
	var map = document.getElementById('map');
	$("#mapLabel").html("<label class='textColor'>Location</label>");
 	var lat=$("#lat").val();
 	var lng=$("#lng").val();

 var coords = {lat: parseFloat(lat), lng: parseFloat(lng)};
  var mapP = new google.maps.Map(map, {
    zoom: 12,
    center: coords
  });
var marker = new google.maps.Marker({
    position: coords,
    map: mapP
  });
}