 var map;
 var markersArray = [];

function initMap()
{
	var latlng = new google.maps.LatLng(48.210033, 16.363449);
	var myOptions = {
        zoom: 5,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
        };
    map = new google.maps.Map(document.getElementById("map"), myOptions);
    google.maps.event.addListener(map, "click", function(event)
        {
            placeMarker(event.latLng);
            $("#lat").val(event.latLng.lat());
			$("#long").val(event.latLng.lng());
        });
}
function placeMarker(location) {
    deleteOverlays();
	var marker = new google.maps.Marker({
                position: location, 
                map: map
    });
    markersArray.push(marker);
}
function deleteOverlays() {
    if (markersArray) {
        for (i in markersArray) {
            markersArray[i].setMap(null);
        }
    markersArray.length = 0;
    }
}

$(function () {
    $('#formPost').validate({
        rules: {
            category: {
                required: true
            },
			title:{
				required: true
			}
        },
        messages: {
            category: {
                required: 'Please select at least one thing.'
            },
title:{
	required: 'Please fill out the field.'
}
        }
    });
});