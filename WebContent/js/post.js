var userPosts=[];
var idUser=0;
var urlPage="http://192.168.1.5:8080/postdetails?id=";
function init(idUserTmp){
	idUser=idUserTmp;
    updateRssPosts();
	updateUserPosts();
    var rssPostInterval = setInterval(updateRssPosts, 30000);
    var userPostInterval = setInterval(updateUserPosts, 30000);
}

function updateRssPosts(){
    $.ajax({
        type: 'GET',
        data: {},
        url: 'data/rsspost',
        dataType: 'json',
        success:function(res){
			var html="";
			$("#rssPost").html(html);
            for(i in res){
				html="";
				html+="<div class='rssPost'>";
				html+="<label class='labelPost'>Title: "+res[i].title+"</label>"
				html+="<label class='labelPost'>Link: <a class='labelA' href='"+res[i].link+"'>"+res[i].link+"</a></label>";
				html+="<label class='labelPost'>Description: "+res[i].description+"</label>";
				html+="<label class='labelPost'>Date: "+res[i].pubDate+"</label>";
				html+="</div>";
				$("#rssPost").append(html);
			}
			
		}
        });
}
function updateUserPosts(){
		$.ajax({
	        type: 'GET',
	        data: {},
	        url: 'data/userpost',
	        dataType: 'json',
	        success:function(res){
			var html="";
			var users=res.users;
			var posts=res.posts;
			if(posts.length > userPosts.length){
				var i=0;
				var postLen=posts.length - userPosts.length;
            	for(i=0;i<postLen;i++){
					html="";
					html+="<div class='rssPost'>";
					
					var user=users.filter(user => user.id === posts[i].userId);
					html+="<div class='form-group infoProfileDiv'>";
					html+='<img class=" imagePostProfile" src="'+user[0].image+'" >';
					html+='<label class="fontSize">'+user[0].name+' '+user[0].surname+'</label>';
					html+='<label class="fontSize dateStyle" >'+posts[i].datetime+'</label>';
					html+='</div>';
					html+="<div class='contentDiv'>";
					if(!(typeof posts[i].text === 'undefined' || posts[i].text ===''))
						html+="<label class='fontSize'>Text: "+posts[i].text+"</label>";
					if(!(typeof posts[i].link === 'undefined' || posts[i].link ===''))
						html+="<label class='fontSize'>Link: <a href='"+posts[i].link+"'>"+posts[i].link+"</a></label>";
					if(!(typeof posts[i].youtube === 'undefined' || posts[i].youtube ==='')){
						html+="<div class='form-group'><label class='fontSize'>Youtube video: </label></div>";
						html+='<iframe class="videoPos" src="https://www.youtube.com/embed/';
						var currentVal=posts[i].youtube;
						var videoId=currentVal.replace("https://www.youtube.com/watch?v=","");
						html+=videoId;
						html+='"></iframe>';
					}
					if(posts[i].video.length > 0 ){
						html+="<div class='form-group'><label class='fontSize'>Video: </label></div>";
						html+='<video class="videoPos" controls> <source src="'+posts[i].video[0]+'"  </source></video>';
					}
					if(posts[i].images.length > 0 ){
						html+="<div class='form-group'><label class='fontSize'>Images: </label></div>";
						for(j in posts[i].images)
						html+='<img class="imgStyle" src="'+posts[i].images[j]+'" >';
					}
					html+="<div style='width:100%'>";
					html+="<div id='shareTwit' >";
					html+="<a class='click'   href='https://twitter.com/share?text=View my post on Ip APP&url="+urlPage+posts[i].id+"' target='_blank'>Tweet</a>";
					html+="</div>";
					html+="<div id='share'>";
					html+="<a class='click' href='https://www.facebook.com/sharer/sharer.php?u="+urlPage+posts[i].id+"' target='_blank'>Share on Facebook</a>";
					html+="</div>";
					
					html+="</div>";
					html+="</div>";
					html+="<div id='comment"+posts[i].id+"'>";
					html+="<button onclick='getComments("+posts[i].id+")' class='slowCommentBtn'>Show comments</button>";
					html+="</div>";
					html+="</div>";
					console.log(userPosts.length);
					if(userPosts.length > 0)
						$("#userPosts").prepend(html);
					else $("#userPosts").append(html);
				}
				userPosts=posts;
			}
			
		}
        });
}
function getComments(id){
	$.ajax({
        type: 'POST',
        data: {id:id},
        url: 'data/comment',
        dataType: 'json',
        success:function(res){
		var html="";
		var comments=res.comments;
		var users=res.users;
		$("#comment"+id).html(html);
		if(comments.length >0){
			for(i in comments){
				html="";
				var user=users.filter(userTmp => userTmp.id === comments[i].userId);
				html+="<div class ='contentDiv'>";
				html+="<label class='fontSizeComment'>"+user[0].name+" "+user[0].surname+" </label>"
				html+="<label style='float:right' class='fontSizeComment'>"+comments[i].datetime +"</label>";
				if(comments[i].type === 'Text')
					html+="<label class='labelVideo'>"+comments[i].content+"<label>";
				else  html+='<div style="width:100%"><img class="imgStyle" src="'+comments[i].content+'" ></div>';
				html+="</div>";
				$("#comment"+id).append(html);
			}
		}else{
				html+="<div class ='contentDiv'>";
				html+="<label class='fontSizeComment'>This post don't have comments. </label>";
				html+="</div>";
				$("#comment"+id).append(html);
		}
		html="";
		html+="<div class='addCommentDiv'>";
		html+="<input id='input"+id+"' placeholder='Add Comment' type='text' class='form-control inputComment' />";
		html+="<div id='imageComment"+id+"'></div>";
		html+="<input id='image"+id+"' accept='image/png, image/jpeg' onchange=' loadImageComment("+id+")' type='file' style='width:100%;margin-bottom:3%;' />";
		html+="<button class='btn btn-success' onclick='addComment("+id+")'>Add Comment</button>";
		html+="</div>";
		html+="<button class='slowCommentBtn' onclick='hideComment("+id+")'>Hide comments</button>";
		$("#comment"+id).append(html);
}
});
}
function hideComment(id){
	var html="<button onclick='getComments("+id+")' class='slowCommentBtn'>Show comments</button>";
	$("#comment"+id).html(html);
}
async function addComment(id){
	var text=$("#input"+id).val();
	var image;
	var images=$("#image"+id).prop("files");
	if(images.length>0){
		image=await readFile(images[0]);
	}
	console.log(id);
	$.ajax({
        type: 'POST',
        data: {idUser:idUser,idPost:id,text:text,image:image},
        url: 'comment',
        dataType: 'json',
        success:function(res){
		$("#input"+id).val("");
		$("#image"+id).val("");
		getComments(id);
}
});
	
}
function loadImageComment(id){
	console.log("ucitava")
	$("#imageComment"+id).html("");
	var reader = new FileReader();
    reader.addEventListener('load', event => {
	console.log("ucitao");
			var img = '<img class="imgStyle" src="'+event.target.result+'" >';
			$("#imageComment"+id).append(img);
			
	});
	var images=$("#image"+id).prop("files");
	reader.readAsDataURL(images[0]);			
}
function logout(){
	const form = document.createElement('form');
  	form.method = 'get';
  	form.action = 'logout';
  	document.body.appendChild(form);
  	form.submit();
}