$.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null) {
       return null;
    }
    return decodeURI(results[1]) || 0;
}
$(document).ready(function(){
	var id=$.urlParam('id');
	$.ajax({
		type:'GET',
		url:'data/userpost?id='+id,
		dataType:'json',
		success:function(res){
					var post=res.post;
					var user=res.user;
					html="";
					html+="<div class='rssPost'>";
					
					html+="<div class='form-group infoProfileDiv'>";
					html+='<img class=" imagePostProfile" src="'+user.image+'" >';
					html+='<label class="fontSize">'+user.name+' '+user.surname+'</label>';
					html+='<label class="fontSize dateStyle" >'+post.datetime+'</label>';
					html+='</div>';
					html+="<div class='contentDiv'>";
					if(!(typeof post.text === 'undefined' || post.text ===''))
						html+="<label class='fontSize'>Text: "+post.text+"</label>";
					if(!(typeof post.link === 'undefined' || post.link ===''))
						html+="<label class='fontSize'>Link: <a href='"+post.link+"'>"+post.link+"</a></label>";
					if(!(typeof post.youtube === 'undefined' || post.youtube ==='')){
						html+="<div class='form-group'><label class='fontSize'>Youtube video: </label></div>";
						html+='<iframe class="videoPos" src="https://www.youtube.com/embed/';
						var currentVal=post.youtube;
						var videoId=currentVal.replace("https://www.youtube.com/watch?v=","");
						html+=videoId;
						html+='"></iframe>';
					}
					if(post.video.length > 0 ){
						html+="<div class='form-group'><label class='fontSize'>Video: </label></div>";
						html+='<video class="videoPos" controls> <source src="'+post.video[0]+'"  </source></video>';
					}
					if(post.images.length > 0 ){
						html+="<div class='form-group'><label class='fontSize'>Images: </label></div>";
						for(j in post.images)
						html+='<img class="imgStyle" src="'+post.images[j]+'" >';
					}
					html+="</div>";

					html+="</div>";
					$("#app").html(html);
					
		}
	});
});