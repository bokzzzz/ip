var oldVal = ""; 
$("#imgInp").change(function(){
        readURL(this);
    });

$(document).ready(function(){
  $("#imgInp").change(function(){
	$("#images").html("");
	$("#video").val("");
	var i=0;
    for(const file of this.files)
		{
			 
			
			var reader = new FileReader();
          	reader.addEventListener('load', event => {
				var img = '<img class="imgStyle" src="'+event.target.result+'" >';
				$('#images').append(img);
          	});
         	 reader.readAsDataURL(file);
		
		i++;
		}
  });
$("#video").change(function(){
	$("#images").html("");
	 $("#imgInp").val("");
	var i=0;
    for(const file of this.files)
		{
			 
			
			var reader = new FileReader();
          	reader.addEventListener('load', event => {
				var video = '<video class="videoStyle" controls> <source src="'+event.target.result+'"  </source></video>';
				$('#images').append(video);
          	});
         	 reader.readAsDataURL(file);
		
		i++;
		}
  });
$("#text").on("change keyup paste",function(){
	console.log("test1");
	var currentVal = $(this).val();
    if(currentVal == oldVal) {
        return; 
    }
	oldVal = currentVal;
	if(currentVal.startsWith("https://www.youtube.com/watch?v=")){
		console.log("test2");
		$("#images").html("");
		$("#imgInp").val("");
		$("#video").val("");
		var videoId=currentVal.replace("https://www.youtube.com/watch?v=","");
		var html='<iframe class="videoStyle" src="https://www.youtube.com/embed/';
		html+=videoId;
		html+='"></iframe>';
		$("#images").html(html);
	}
  });
});

async function addPost(id){
	const filesImg=$("#imgInp").prop("files");
	const filesVideo=$("#video").prop("files");
	
	if(filesImg.length >0){
		var imagesStr=[];
		var text=$("#text").val();
      	for (let file of filesImg) {
			 var contents = await readFile(file);
			imagesStr.push(contents);
		}
   
		sendPost({text:text,id:id,images:imagesStr});
	}
	else if(filesVideo.length>0){
		var videoStr=[];
		var text=$("#text").val();
      	for (let file of filesVideo) {
			 var contents = await readFile(file);
			videoStr.push(contents);
		}
   
		sendPost({text:text,id:id,videos:videoStr});
	}
	else{
		var text=$("#text").val();
		if(text.length > 0)
		sendPost({text:text,id:id});
	}
}
function readFile(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();

    reader.onload = res => {
      resolve(res.target.result);
    };
    reader.onerror = err => reject(err);

    reader.readAsDataURL(file);
  });
}
function sendPost(data){
	console.log("sendujem post")
	console.log(data);
	$.ajax({
  	type: "POST",
  	url: 'userpost',
 	data: data,
	dataType: 'JSON',
  	success: function(res){
		$("#images").html("");
		$("#imgInp").val("");
		$("#video").val("");
		$("#text").val("");
		updateUserPosts();
}
});
}
