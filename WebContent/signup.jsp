<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
	integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z"
	crossorigin="anonymous">
</head>
<body>
	<div class="login">
		<div class="container">
			<form method="POST" action="signup">
			<div class="input-group mb-3 signUpH2">
					<h2>Sign Up</h2>
				</div>
				<div class="input-group mb-3">
					<div class="input-group-prepend">
						<span class="input-group-text" id="basic-addon1">@</span>
					</div>
					<input type="text" class="form-control" placeholder="Enter your username"
						name="username" required>
				</div>
				<div class="input-group mb-3">
					<input type="text" class="form-control"
						placeholder="Enter your name" name="name" required>
				</div>
				<div class="input-group mb-3">
					<input type="text" class="form-control"
						placeholder="Enter your surname" name="surname" required>
				</div>
				<div class="input-group mb-3">
					<input type="password" class="form-control"
						placeholder="Enter your password" name="password" required>
				</div>
				<div class="input-group mb-3">
					<input type="password" class="form-control"
						placeholder="Repeat password" name="repassword" required>
				</div>
				<div class="input-group mb-3">
					<input type="email" class="form-control"
						placeholder="Enter your email" name="email" required>
				</div>
				<div class="form-group">
					<%=request.getSession().getAttribute("resultSignUp") != null ? request.getSession().getAttribute("resultSignUp") : ""%>
				</div>
				<div class="middleItem">
					<button type="submit" class="btn btn-success loginButtonStyle">Sign Up</button>
				</div>
			</form>
		</div>
	</div>



	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
		integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
		crossorigin="anonymous"></script>
	<script
		src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
		integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
		integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
		crossorigin="anonymous"></script>
</body>
</html>