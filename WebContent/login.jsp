<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%
	if(request.getSession().getAttribute("userData") != null){
		request.getRequestDispatcher("/home").forward(request, response);
	}
%>
<html>
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
	integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z"
	crossorigin="anonymous">

<title>Login</title>
</head>
<body>
	<div class="login">
		<div class="container">
			<div class=" form-group imgPos">
				<img src="images/loginIcon.png" class="img-fluid imgLogin">
			</div>
			<form method="POST" action="login">

				<div class="form-group">
					<label for="email">Username</label> <input type="text"
						placeholder="Enter Username" name="username" class="form-control"
						id="username" required>
				</div>
				<div class="form-group">
					<label for="password">Password</label> <input type="password"
						placeholder="Enter Password" class="form-control" id="password" name="password"
						required>
				</div>
				<div class="form-group">
					<%=request.getSession().getAttribute("result") != null ? request.getSession().getAttribute("result") : ""%>
				</div>
				<div class="middleItem">
					<button type="submit" class="btn btn-success loginButtonStyle">Login</button>
				</div>
			</form>
			<div class="form-group signup">
			Don't have an account yet?<a href="signup.jsp"> Sign up</a>
			</div>
		</div>
	</div>

	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
		integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
		crossorigin="anonymous"></script>
	<script
		src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
		integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
		integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
		crossorigin="anonymous"></script>
</body>
</html>