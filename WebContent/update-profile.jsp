<%@page import="dto.UserDTO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="update-profile.js"></script>
<link rel="stylesheet" href="css/style-profile.css">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
	integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z"
	crossorigin="anonymous">
<meta charset="ISO-8859-1">
<title>Update profile</title>
</head>
<body onload="onLoad()">
	<div class="poss">
		<div class=" form-group imgDiv">
			<%
				UserDTO user = (UserDTO) request.getSession().getAttribute("user");
			%>
			<img src="images/user.png" class="img-fluid " id="avatar">
		</div>
		<div class=" form-group " style="margin-top: 1">
			<label><%="Username: " + user.getUsername()%></label>
		</div>
		<div class=" form-group ">
			<label><%="Name and Surname: " + user.getName() + " " + user.getSurname()%></label>
		</div>
		<div class=" form-group ">
			<label><%="Email: " + user.getEmail()%></label>
		</div>

	</div>
	<div class="poss2">
		<div class=" form-group ">
			<h2>Profile Info</h2>
		</div>
		<form action="update" method="POST" enctype="multipart/form-data">
			<div class=" form-group ">

				<select class="custom-select optionSize" id="country" name="country"
					onchange="onChangeCounty()" required>
					<option value="" disabled selected>Select your country</option>
				</select>
			</div>
			<div class=" form-group ">
				<select class="custom-select optionSize" id="region" name="region"
					onchange="onChangeRegion()" required>
					<option value="" disabled selected>Select your region</option>
				</select>
			</div>
			<div class=" form-group ">
				<select class="custom-select optionSize" id="city" name="city" required>
					<option value="" disabled selected>Select your city</option>
				</select>
			</div>
			<div class=" form-group ">
				<label>Select avatar:</label>
			</div>
			<div class="form-group ">
				<input placeholder="Choose Image" type="file" id="avatar"
					name="avatar" accept="image/png, image/jpeg"
					onchange="onChooseAvatar()"> <input id="avatarUrl"
					type="text" name="avatarUrl" value="none" hidden>
			</div>
			<div class="form-group "></div>
			<input type="checkbox" id="app" name="app" value="app">
			<label for=""> Notification by application</label> <br> <input
				type="checkbox" id="vehicle2" name="email" value="email"> <label
				for="email"> Notification by email</label><br>
			<div class=" form-group ">
				<button type="submit" class="btn btn-success saveButtonStyle">Save</button>
			</div>
			<div id="demo"></div>
		</form>
	</div>







	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
		integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
		crossorigin="anonymous"></script>
	<script
		src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
		integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
		integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
		crossorigin="anonymous"></script>
</body>
</html>