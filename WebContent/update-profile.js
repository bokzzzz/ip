


var isAvatarChoose=false;
var flags=[];
function onLoad(){
	isAvatarChoose=false;
	var xhttp;
	var x;
  	xhttp = new XMLHttpRequest();
  	xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
		var returnedData = JSON.parse(this.responseText);
		for (i in returnedData) {
  			x += "<option value="+returnedData[i].alpha2Code+">" + returnedData[i].name + "</option>";
			flags.push(returnedData[i].flag);
		}
		document.getElementById("country").innerHTML += x;
    }
  };
  xhttp.open("GET", "https://restcountries.eu/rest/v2/region/europe", true);
  xhttp.send();

}
function onChangeCounty(){
if(!isAvatarChoose){
	var output = document.getElementById('avatar');
	output.src = flags[$("#country").prop('selectedIndex') -1];
	$("#avatarUrl").val(output.src);
}
 JsonHttpRequest("http://battuta.medunes.net/api/region/"+$( "#country" ).val()+"/all/?key=963b745bbbb374516c3825af654ba7e5&callback=cb", "cb", "region");
}


function onChangeRegion(){

 JsonHttpRequest("http://battuta.medunes.net/api/city/" + $( "#country" ).val() + "/search/?region=" + $( "#region" ).val() + "&key=963b745bbbb374516c3825af654ba7e5&callback=cb", "cb", "city");
}

function JsonHttpRequest(url, callback, selectName) {
	var e = document.createElement('script');
	e.src = url;
	document.head.appendChild(e);

	window[callback] = (data) => {
		
		var select = null;
		if (selectName == "region") {
			var html = "<option value=0 disabled selected>Select your region..</option>";
			select = document.getElementById("region");
			
			for (i in data) {
  			html += "<option value='"+data[i].region+"'>" + data[i].region + "</option>";
			
		}
		}
		else {
			var html = `<option value=0 disabled selected>Select your city..</option>`;
			select = document.getElementById("city");
		
			for (i in data) {
  			html += "<option value='"+data[i].city+"'>" + data[i].city + "</option>";
			
		}
		}
		select.innerHTML = html;
	}
}
function onChooseAvatar(){
	var output = document.getElementById('avatar');
	output.src = URL.createObjectURL(event.target.files[0]);
	output.onload = function() {
		URL.revokeObjectURL(output.src) 
	isAvatarChoose=true;
	$("#avatarUrl").val("upload");
	}

}
