<%@page import="models.RSSPost"%>
<%@page import="rss.FeedMessage"%>
<%@page import="dto.UserDTO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width,initial-scale=1">
<title>Home</title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="weather.js"></script>
<script src="js/post.js"></script>
<script src="js/notification.js"></script>
<script src="js/userPost.js"></script>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
	integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/home.css">
<link rel="stylesheet" href="css/home-profile.css">
<link rel="stylesheet" href="css/home-post.css">
</head>
<body>
	<%
		UserDTO user = (UserDTO) request.getSession().getAttribute("userData");
	%>

	<div id="app" class="appDiv">
		<div id="profile" class="profileDiv">
			<div class="infoDiv">
				<label class="labelInfo"><%=user.getUsername()%></label> <img
					src="<%=user.getImage()%>"
					class="rounded-circle img-fluid imgProfile" alt="Cinque Terre">
				<label class="labelInfo"><%=user.getName() + " " + user.getSurname()%></label>
				<label class="labelInfo"><%="Number of logins:"+user.getNumberOfLogins()%></label>


			</div>
			<div class="labelNotif">
				<Label class="notifLabel">Notification:</label>
			</div>
			<div id="notificationDiv" class="notificationDiv"></div>
		
		</div>
		<div id="main" class="mainDiv">
			<div id="post" class="addPostDiv">
				<form method="get" action="addWarning">
					<div class=" form-group addWarning ">
						<button type="submit" class="saveButtonStyle btn btn-success ">
							Add Warning</button>
					</div>
				</form>
				<div class="orLabel">
					<label>Or</label>
				</div>

				<div id="userPost" class="addUserPostDiv">
					<!-- <form method="POST"  action="userpost" enctype="multipart/form-data"> -->
					<div class=" form-group ">
						<textarea class="form-control" name="text" id="text" rows="3"
							placeholder="Insert text or link or youtube video link"></textarea>
					</div>
					<div id="images"></div>
					<div class="labelVideo">
						<label class="imgLabelVideo">Choose images </label> <label
							class="orLabelVideo">Or </label> <label class="videoLabel">Choose
							video </label>
					</div>
					<div class=" form-group ">
						<input class="imgInput" type="file" id="imgInp" name="images"
							accept="image/png, image/jpeg" multiple="multiple"> <input
							class="videoInput" type="file" id="video" name="video"
							accept="video/mov, video/ogg,video/mp4">
					</div>
					<div class=" form-group ">
						<button onclick="addPost(<%=user.getId()%>)"
							class="btn btn-success">Add Post</button>
					</div>
				</div>
			</div>
			<div id="userPosts"></div>
			<div id="rssPost"></div>

		</div>
		<div  class="weatherDiv">
		<div  id="logout">
		<div class=" form-group ">
						<button style="width:80%;margin: 2% 10% 0 10%;" onclick="logout()"
							class="btn btn-success">Logout</button>
					</div>
		</div>
		<div id="weather"></div>
		</div>
	</div>

	<script type="text/javascript">
	 init(<%=user.getId()%>);
	 
	setWeather(<%="'" + user.getCountry() + "'"%>,<%="'" + user.getRegion() + "'"%>
		);
	setNotification(<%="'" + user.getNotification() + "'"%>);
				
	</script>

</body>
</html>