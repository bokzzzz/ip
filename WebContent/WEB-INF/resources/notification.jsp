<%@page import="dto.UserDTO"%>
<%@page import="dto.WarningPostDTO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Notification</title>
 <script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
	<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
	integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z"
	crossorigin="anonymous">
  <script type="text/javascript"
          src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCqJ-5FwLeDRrfcIZLBxshb-KJ7oRS07gQ"></script>
  <script src="https://unpkg.com/location-picker/dist/location-picker.min.js"></script>
   <script type="text/javascript" src="js/maps.js"></script>
   <script type="text/javascript" src="js/notification.js"></script>
  <link rel="stylesheet" href="css/warning.css">
</head>
<body>
	<%
	WarningPostDTO post=(WarningPostDTO)request.getSession().getAttribute("post");
	UserDTO user=(UserDTO)request.getSession().getAttribute("userCreator");
	String categories=(String)request.getSession().getAttribute("categories");
	%>
	<div id="post" class="mainDiv">
		<div class="form-group ">
			<label class="titleLabel textColor">View Warning Post</label>
		</div>
		<label class="textColor">Title</label>
		<div class="input-group mb-3">
					<input id="title" type="text" class="form-control" value="<%= post.getTitle() %>"
						 name="title" readonly="readonly">
		</div>
		<label class="textColor">Creator</label>
		<div class="input-group mb-3">
					<input id="name" type="text" class="form-control" value="<%= user.getName() + " " + user.getSurname() %>"
					 name="name" readonly="readonly">
		</div>
		<label class="textColor">Latitude</label>
		<div class="input-group mb-3">
					<input id="lat" type="text" class="form-control" value="<%= post.getLat() %>"
					 name="lng" readonly="readonly">
		</div>
		<label class="textColor">Longitude</label>
		<div class="input-group mb-3">
					<input id="lng" type="text" class="form-control" value="<%= post.getLng() %>"
					 name="lng" readonly="readonly">
		</div>
		<label class="textColor">Categories</label>
		<div class="input-group mb-3">
					<input id="cat" type="text" class="form-control" value="<%= categories  %>"
					 name="cat" readonly="readonly">
		</div>
		<div id="mapLabel">
		</div>
		
		<div id="map"></div>
	</div>
	<script>
  <%if(!"0".equals(post.getLng()) && !"0".equals(post.getLat())) {%>
 	setMapStat();
 	<%}%>
</script>
</body>
</html>