<%@page import="models.WarningCategory"%>
<%@page import="dto.WarningCategoryDTO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
 <title>Example</title>
 <script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
	<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
	integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z"
	crossorigin="anonymous">
  <script type="text/javascript"
          src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCqJ-5FwLeDRrfcIZLBxshb-KJ7oRS07gQ"></script>
  <script src="https://unpkg.com/location-picker/dist/location-picker.min.js"></script>
   <script type="text/javascript" src="js/maps.js"></script>
  <link rel="stylesheet" href="css/warning.css">
<body>
	<div id="app">
	
	<form id="formPost" action="addWarning" method="post">
		<div id="post" class="mainDiv">
		<div class="form-group ">
			<label class="titleLabel textColor">Add Warning Post</label>
		</div>
		<div class="input-group mb-3">
					<input id="title" type="text" class="form-control"
						placeholder="Enter title" name="title" required>
				</div>
			<div class=" form-group ">
				<select class="custom-select optionSize" id="category" name="category" multiple="multiple"  required>
					<option value="" disabled selected>Select category</option>
					<%
					for(WarningCategoryDTO category:new WarningCategory().getCategories()){
						out.println("<option value=\""+category.getId()+"\">"+category.getName()+"</option>");
					}
					%>
				</select>
			</div>
			<div class="form-group ">
			<input type="checkbox" id="urgent" name="urgent" value="1">
			<label class="textColor" for="">Urgent</label> 
			</div>
			
			<input id="lat" name="lat" value="0" type="hidden">
			<input id="long" name="long" value="0" type="hidden">
			<label class="textColor">Select location:</label>
			<div class=" form-group ">
			<div id="map"></div>
			</div>
			<div class=" form-group ">
				<button type="submit" class="btn btn-success saveButtonStyle">Add Warning</button>
			</div>
		</div>
		</form>
	</div>
		
<script>
 	initMap();
</script>

</body>
</html>