<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>User post</title>
<meta property="og:url"           content="https://192.168.1.5:8080/" />
<meta property="og:type"          content="website" />
<meta property="og:title"         content="IP Application" />
<meta property="og:description"   content="View user post on IP APP" />
<meta property="og:image"         content="https://img.freepik.com/free-vector/blank-warning-sign_97458-36.jpg?size=626&ext=jpg" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="js/post.js"></script>
<script src="js/notification.js"></script>
<script src="js/userPost.js"></script>
<script src="js/viewPost.js"></script>
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/sr_RS/sdk.js#xfbml=1&version=v8.0" nonce="OXDVAMFD"></script>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
	integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/home-post.css">
</head>
<body class="bodyStyle">
	<div id="app">
	</div>
</body>
</html>