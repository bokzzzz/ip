package listeners;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;

import dao.UserDAO;
import dto.UserDTO;


/**
 * Application Lifecycle Listener implementation class SessionListener
 *
 */
@WebListener
public class SessionListener implements HttpSessionAttributeListener {

	@Override
	public void attributeRemoved(HttpSessionBindingEvent event) {
		if("userData".equals(event.getName())){
			UserDTO user=(UserDTO)event.getValue();
			user.setIsLoggedIn(0);
			new UserDAO().updateLoggedInStatus(user);
		}
	}

	@Override
	public void attributeAdded(HttpSessionBindingEvent event) {
	}

   
	
}
