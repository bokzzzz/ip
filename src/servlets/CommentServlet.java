package servlets;

import java.io.IOException;
import java.sql.ResultSet;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import dao.CommentDAO;
import dto.CommentDTO;
import dto.UserPostDTO;

/**
 * Servlet implementation class CommentServlet
 */
@WebServlet("/comment")
public class CommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CommentServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("login.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getSession().getAttribute("userData") != null) {
		Enumeration<String> params = request.getParameterNames();
		int idUser = 0;
		int idPost = 0;
		String text = null;
		String image=null;
		while (params.hasMoreElements()) {
			String elem = params.nextElement();
			if("text".equals(elem)) {
				text=request.getParameter("text");
			}
			else if("image".equals(elem)) {
				image=request.getParameter("image");
			}
			else if("idUser".equals(elem)) {
				idUser=Integer.parseInt(request.getParameter("idUser"));
			}
			else if("idPost".equals(elem)) {
				idPost=Integer.parseInt(request.getParameter("idPost"));
			}
		}
		CommentDAO commentDAO=new CommentDAO(); 
		CommentDTO comment=new CommentDTO();
		if(image != null && idUser > 0 && idPost > 0) {
			comment.setPostId(idPost);
			comment.setUserId(idUser);
			comment.setContent(image);
			comment.setType("Image");
			commentDAO.addComment(comment);
		} 
		if(text != null && idUser > 0 && idPost > 0 ) {
			comment.setPostId(idPost);
			comment.setUserId(idUser);
			comment.setContent(text);
			comment.setType("Text");
			commentDAO.addComment(comment);
		}
		response.getWriter().write(new Gson().toJson("Sucess!"));
	}else {
		response.sendRedirect("login.jsp");
	}
	
	}

}
