package servlets;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import dto.UserDTO;
import models.User;

/**
 * Servlet implementation class UpdateProfile
 */
@WebServlet("/update")
@MultipartConfig
public class UpdateProfile extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UpdateProfile() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String avatarUrl = request.getParameter("avatarUrl");
		String imageUrl="";
		UserDTO userDto=(UserDTO) request.getSession().getAttribute("user");
		 if ("upload".equals(avatarUrl)) {
			Part part = request.getPart("avatar");
			byte[] avatarBytes = part.getInputStream().readAllBytes();
			String avatarPath = getServletContext().getRealPath("WEB-INF/userAvatar") + File.separator + userDto.getUsername()
					+"_"+ part.getSubmittedFileName();
			Path path = Path.of(avatarPath);
			Files.write(path, avatarBytes, StandardOpenOption.CREATE);
			imageUrl=userDto.getUsername()+ part.getSubmittedFileName();
		}
		 else if(!"none".equals(avatarUrl)){
			 try(InputStream in = new URL(avatarUrl).openStream()){
				    Files.copy(in, Paths.get(getServletContext().getRealPath("WEB-INF/userAvatar") + File.separator + userDto.getUsername()+"_"+".svg"));
				    imageUrl=userDto.getUsername()+"_"+".svg";
				}
		 }
		 String country=request.getParameter("country");
		 String region=request.getParameter("region");
		 String city=request.getParameter("city");
		 String notificationByEmail=request.getParameter("email");
		 String notificationByApp=request.getParameter("app");
		 StringBuilder notification=new StringBuilder();
		 notification.append(notificationByApp != null? notificationByApp:"" );
		 notification.append(notificationByEmail != null?" " +notificationByEmail:"" );
		 userDto.setCountry(country);
		 userDto.setRegion(region);
		 userDto.setNotification(notification.toString());	
		 userDto.setCity(city);
		 userDto.setImage(imageUrl);
		 User user=new User();
		 user.setUserDTO(userDto);
		 if(user.updateInfo()) {
			 response.sendRedirect("login.jsp");
		 }
		 else {
			 request.getSession().setAttribute("resultUpdateInfo", "Update is not working!");
				RequestDispatcher dispatcher = request.getRequestDispatcher("update-profile.jsp");
				dispatcher.forward(request, response);
		 }
	}

}
