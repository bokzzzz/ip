package servlets;

import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dto.UserDTO;
import models.User;

/**
 * Servlet implementation class Login
 */
@WebServlet("/login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username= request.getParameter("username");
		String password=request.getParameter("password");
		if(username == null || password == null || username.isEmpty() || password.isEmpty()) {
			request.getSession().setAttribute("result", "Username or password are incorrect!");
			RequestDispatcher dispatcher = request.getRequestDispatcher("login.jsp");
			dispatcher.forward(request, response);
			return;
		}
		User user=new User();
		user.getUserDTO().setUsername(username);
		user.getUserDTO().setPassword(password);
		UserDTO userDTO=user.checkLogin();
		if(userDTO != null) {
			StringBuilder imgSrc=new StringBuilder();
			imgSrc.append("data:image/");
			String imgUrl=userDTO.getImage();
			if(imgUrl.contains(".svg")) {
				imgSrc.append("svg+xml");
			}
			else {
				String[] splits=imgUrl.split("[.]");
				imgSrc.append(splits[splits.length-1]);
			}
			imgSrc.append(";base64,");
			InputStream img=getServletContext().getResourceAsStream("/WEB-INF/userAvatar/"+imgUrl);
			imgSrc.append(Base64.getEncoder().encodeToString(img.readAllBytes()));
			userDTO.setImage(imgSrc.toString());
			request.getSession().setAttribute("userData",userDTO);
			response.sendRedirect("home");
		}
		else {
			request.getSession().setAttribute("result", "Username or password are incorrect!");
			RequestDispatcher dispatcher = request.getRequestDispatcher("login.jsp");
			dispatcher.forward(request, response);
			
		}
	}

}
