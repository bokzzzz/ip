package servlets;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import dao.UserPostDAO;
import dto.UserPostDTO;

/**
 * Servlet implementation class UserPostServlet
 */
@WebServlet("/userpost")
@MultipartConfig
public class UserPostServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserPostServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.sendRedirect("login.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Enumeration<String> params = request.getParameterNames();
		int id = 0;
		String text = null;
		UserPostDTO post = new UserPostDTO();
		while (params.hasMoreElements()) {
			String elem = params.nextElement();
			if ("images[]".equals(elem)) {
				String[] images = request.getParameterValues("images[]");
				post.setImages(images);

			} else if ("videos[]".equals(elem)) {
				String[] videos = request.getParameterValues("videos[]");
				post.setVideo(videos);

			} else if ("id".equals(elem)) {
				String idStr = request.getParameter("id");
				id = Integer.parseInt(idStr);
				post.setUserId(id);
			} else {
				text = request.getParameter("text");
				if (text != null && text.startsWith("https://www.youtube.com/watch?v=")) {
					post.setYoutube(text);
				} else if (text != null && (text.startsWith("https://") || text.startsWith("http://"))) {
					post.setLink(text);
				} else {
					post.setText(text);
				}
			}
		}
		UserPostDAO postDAO = new UserPostDAO();
		boolean isAdded=true;
		if (postDAO.addUserPost(post)) {
			
			if (post.getImages() != null) {
				for (String img : post.getImages()) {
					if(!postDAO.addImagesToUserPost(post.getId(), img)) {
						isAdded=false;
					}
				}
			}
			if (post.getVideo() != null) {
				for (String vid : post.getVideo())
					if(!postDAO.addVideoToUserPost(post.getId(), vid))
						isAdded=false;
			}
		}else isAdded=false;
		if(isAdded)
			response.getWriter().write(new Gson().toJson("Post successfully added."));
		else response.getWriter().write(new Gson().toJson("Something is wrong with server."));
	}

}
