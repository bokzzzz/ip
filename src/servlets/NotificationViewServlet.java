package servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDAO;
import dao.WarningCategoryDAO;
import dao.WarningPostDAO;
import dto.UserDTO;
import dto.WarningPostDTO;

/**
 * Servlet implementation class NotificationViewServlet
 */
@WebServlet("/notification")
public class NotificationViewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public NotificationViewServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.sendRedirect("login.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (request.getSession().getAttribute("userData") != null) {
			String idStr = request.getParameter("id");
			if (idStr != null) {
				Integer id = Integer.parseInt(idStr);
				WarningPostDTO post = new WarningPostDAO().getWarningPostById(id);
				StringBuilder categories = new StringBuilder();
				WarningCategoryDAO categoryDAO = new WarningCategoryDAO();
				for (Integer idCat : post.getCategoriesId())
					categories.append(categoryDAO.getNameById(idCat) + " ,");
				categories.deleteCharAt(categories.length() - 1);
				UserDTO userCreator = new UserDAO().getUserById(post.getUserId());
				request.getSession().setAttribute("userCreator", userCreator);
				request.getSession().setAttribute("categories", categories.toString());
				request.getSession().setAttribute("post", post);
				RequestDispatcher dispecher = request.getRequestDispatcher("/WEB-INF/resources/notification.jsp");
				dispecher.forward(request, response);
			}
		} else {
			response.sendRedirect("login.jsp");
		}
	}

}
