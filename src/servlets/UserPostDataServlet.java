package servlets;

import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import dao.UserDAO;
import dao.UserPostDAO;
import dto.UserDTO;
import models.JsonOneUserPost;
import models.JsonUserPost;

/**
 * Servlet implementation class UserPostDataServlet
 */
@WebServlet("/data/userpost")
public class UserPostDataServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserPostDataServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (request.getSession().getAttribute("userData") != null) {
			String idStr = request.getParameter("id");
			UserPostDAO usersPostDAO = new UserPostDAO();
			if (idStr != null) {
				int id = Integer.parseInt(idStr);
				JsonOneUserPost oneUserPost = new JsonOneUserPost();
				oneUserPost.setPost(usersPostDAO.getUserPostById(id));
				oneUserPost.setUser(new UserDAO().getUserById(oneUserPost.getPost().getUserId()));
				setUserImage(oneUserPost.getUser());
				Gson gson = new GsonBuilder().disableHtmlEscaping().create();
				response.getWriter().write(gson.toJson(oneUserPost));
			} else {
				JsonUserPost jsonPost = new JsonUserPost();

				jsonPost.setPosts(usersPostDAO.getAllUserPost());
				jsonPost.setUsers(usersPostDAO.getAllUsersFromPost());
				for (UserDTO user : jsonPost.getUsers())
					setUserImage(user);
				Gson gson = new GsonBuilder().disableHtmlEscaping().create();
				response.getWriter().write(gson.toJson(jsonPost));
			}
		} else
			response.sendRedirect("login.jsp");
	}

	private void setUserImage(UserDTO user) {
		StringBuilder imgSrc = new StringBuilder();
		imgSrc.append("data:image/");
		String imgUrl = user.getImage();
		if (imgUrl.contains(".svg")) {
			imgSrc.append("svg+xml");
		} else {
			String[] splits = imgUrl.split("[.]");
			imgSrc.append(splits[splits.length - 1]);
		}
		imgSrc.append(";base64,");
		InputStream img = getServletContext().getResourceAsStream("/WEB-INF/userAvatar/" + imgUrl);
		try {
			imgSrc.append(Base64.getEncoder().encodeToString(img.readAllBytes()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		user.setImage(imgSrc.toString());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
