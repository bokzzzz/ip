package servlets;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import dao.CommentDAO;
import models.JsonComment;

/**
 * Servlet implementation class CommentDataServlet
 */
@MultipartConfig
@WebServlet("/data/comment")
public class CommentDataServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CommentDataServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.sendRedirect("login.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String idStr = request.getParameter("id");
		if (request.getSession().getAttribute("userData") != null) {
			if (idStr != null) {
				Integer id = Integer.parseInt(idStr);
				Gson gson = new GsonBuilder().disableHtmlEscaping().create();
				CommentDAO commentDAO = new CommentDAO();
				JsonComment comments = new JsonComment();
				comments.setComments(commentDAO.getAllCommentByPostId(id));
				comments.setUsers(commentDAO.getAllUsersFromCommentsById(id));
				response.getWriter().write(gson.toJson(comments));
			}
		}
		else {
			response.sendRedirect("login.jsp");
		}
	}

}
