package servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dto.UserDTO;
import models.User;

/**
 * Servlet implementation class SignUp
 */
@WebServlet("/signup")
public class SignUp extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SignUp() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username=request.getParameter("username");
		String name=request.getParameter("name");
		String surname=request.getParameter("surname");
		String password=request.getParameter("password");
		String repassword=request.getParameter("repassword");
		String email=request.getParameter("email");
		if(username == null || username.isEmpty() || name == null || name.isEmpty() || 
				surname == null || surname.isEmpty() || password == null || password.isEmpty() 	||
				repassword == null || repassword.isEmpty() || email == null || email.isEmpty() ) {
			request.getSession().setAttribute("resultSignUp", "Please fill out all fields!");
			RequestDispatcher dispatcher = request.getRequestDispatcher("signup.jsp");
			dispatcher.forward(request, response);
			return;
		}
		if(!password.equals(repassword)) {
			request.getSession().setAttribute("resultSignUp", "Password and repeat password must be the same!");
			RequestDispatcher dispatcher = request.getRequestDispatcher("signup.jsp");
			dispatcher.forward(request, response);
			return;
		}
		User user = new User();
		UserDTO userDTO= new UserDTO();
		userDTO.setUsername(username);
		userDTO.setName(name);
		userDTO.setSurname(surname);
		userDTO.setPassword(repassword);
		userDTO.setEmail(email);
		user.setUserDTO(userDTO);
		String message=user.signUp();
		if(message.contains("successfully")) {
			request.getSession().setAttribute("user", userDTO);
			response.sendRedirect("update-profile.jsp");
			return;
		}
		request.getSession().setAttribute("resultSignUp", message);
		RequestDispatcher dispatcher = request.getRequestDispatcher("signup.jsp");
		dispatcher.forward(request, response);
	}

}
