package models;

import java.util.List;

import dto.UserDTO;
import dto.UserPostDTO;

public class JsonUserPost {
	private List<UserPostDTO> posts;
	private List<UserDTO> users;
	public JsonUserPost() {
		super();
		// TODO Auto-generated constructor stub
	}
	public List<UserPostDTO> getPosts() {
		return posts;
	}
	public void setPosts(List<UserPostDTO> posts) {
		this.posts = posts;
	}
	public List<UserDTO> getUsers() {
		return users;
	}
	public void setUsers(List<UserDTO> users) {
		this.users = users;
	}
}
