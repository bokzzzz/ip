package models;

import java.util.List;

import rss.Feed;
import rss.FeedMessage;
import rss.RSSFeedParser;

public class RSSPost {
	public List<FeedMessage> getFeedMessages(){
		RSSFeedParser parser = new RSSFeedParser("https://europa.eu/newsroom/calendar.xml_en?field_nr_events_by_topic_tid=151");
		Feed feed = parser.readFeed();
		return feed.getMessages();
	}
}
