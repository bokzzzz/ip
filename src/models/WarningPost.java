package models;

import dao.WarningPostDAO;
import dto.WarningPostDTO;

public class WarningPost {
	public boolean addWarningPost(WarningPostDTO post) {
		WarningPostDAO postDAO=new WarningPostDAO();
		if(postDAO.addWarningPost(post)) {
			for(Integer catId:post.getCategoriesId()) {
				if(!postDAO.addCategoryToWarningPost(post.getId(), catId)) {
					return false;
				}
			}
			if(post.getUrgent() == 1)
				new PostEmail().sendEmailToUsers(post);
			return true;
		}
		return false;
	}
}
