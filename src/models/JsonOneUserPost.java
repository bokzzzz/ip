package models;

import dto.UserDTO;
import dto.UserPostDTO;

public class JsonOneUserPost {
	private UserDTO user;
	private UserPostDTO post;
	public UserDTO getUser() {
		return user;
	}
	public void setUser(UserDTO user) {
		this.user = user;
	}
	public UserPostDTO getPost() {
		return post;
	}
	public void setPost(UserPostDTO post) {
		this.post = post;
	}
	public JsonOneUserPost() {
		super();
	}
}
