package models;

import java.util.List;

import dao.UserDAO;
import dao.WarningCategoryDAO;
import dto.UserDTO;
import dto.WarningPostDTO;
import mail.MailSender;

public class PostEmail {
	
	public void sendEmailToUsers(WarningPostDTO post) {
		List<UserDTO> usersWithEmailNotif=new UserDAO().getAllUsersWithEmailNotification(post.getUserId());
		String subject="Warning: "+post.getTitle();
		StringBuilder text=new StringBuilder();
		text.append(", new warning is here. Place:longitude:"+post.getLng()+" latitude: "+post.getLat()+
				", Categories:");
		WarningCategoryDAO categoryDAO=new WarningCategoryDAO();
		for(Integer catId:post.getCategoriesId())
			text.append(" "+categoryDAO.getNameById(catId)+",");
		text.deleteCharAt(text.length()-1);
		for(UserDTO user:usersWithEmailNotif) {
			String sendText="Dear "+user.getName()+" "+user.getSurname()+text.toString();
			MailSender.send(sendText, subject, user.getEmail());
		}
	}
}
