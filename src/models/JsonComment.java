package models;

import java.util.List;

import dto.CommentDTO;
import dto.UserDTO;

public class JsonComment {
	private List<CommentDTO> comments;
	private List<UserDTO> users;
	public List<UserDTO> getUsers() {
		return users;
	}
	public void setUsers(List<UserDTO> users) {
		this.users = users;
	}
	public JsonComment() {
		super();
	}
	public List<CommentDTO> getComments() {
		return comments;
	}
	public void setComments(List<CommentDTO> comments) {
		this.comments = comments;
	}
}
