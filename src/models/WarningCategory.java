package models;

import java.util.List;

import dao.WarningCategoryDAO;
import dto.WarningCategoryDTO;

public class WarningCategory {
	public List<WarningCategoryDTO> getCategories(){
		return new WarningCategoryDAO().getAllCategory();
	}
}
