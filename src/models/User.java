package models;

import dao.UserDAO;
import dao.UserLoginDAO;
import dto.UserDTO;

public class User {
	private UserDTO userDTO=new UserDTO();
	public UserDTO getUserDTO() {
		return userDTO;
	}
	public void setUserDTO(UserDTO userDTO) {
		this.userDTO = userDTO;
	}
	public UserDTO checkLogin() {
		UserDTO user=new UserDAO().getUserByUsernameAndPassword(userDTO.getUsername(), userDTO.getPassword());
		if(user != null) {
			if("Active".equals(user.getStatus())) {
				user.setIsLoggedIn(1);
				UserDAO userDAO= new UserDAO();
				userDAO.updateLoggedInStatus(user);
				userDAO.updateNumberOfLogins(user);
				new UserLoginDAO().addUserLoginTime(user);
				user.setNumberOfLogins(user.getNumberOfLogins()+1);
				return user;
			}
		}
		return null;
	}
	public String signUp() {
		StringBuilder message=new StringBuilder();
		UserDAO userDAO=new UserDAO();
		if(userDAO.isUsernameExist(userDTO.getUsername())) {
			message.append("Username already exist!");
			return message.toString();
		}
		if(userDAO.isEmailExist(userDTO.getEmail())) {
			message.append("Email already exist!");
			return message.toString();
		}
		boolean isAdded=userDAO.addUser(userDTO);
		if(isAdded)
			message.append("User successfully added");
		else
			message.append("User did not add");
		return message.toString();
	}
	public boolean updateInfo() {
		UserDAO userDAO=new UserDAO();
		return userDAO.updateInfo(userDTO);
	}
}
