package mail;


import java.util.Properties;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


public class MailSender {
    static private String mailFrom;
    static private String username;
    static private String password;
    static private Properties props;
    static {
        props = new Properties();
		ResourceBundle bundle = PropertyResourceBundle.getBundle("mail.mail");
		props.put("mail.smtp.auth", bundle.getString("mail.smtp.auth"));		
		props.put("mail.smtp.ssl.enable", bundle.getString("mail.smtp.ssl.enable"));		
		props.put("mail.smtp.host", bundle.getString("mail.smtp.host"));		
		props.put("mail.smtp.port", bundle.getString("mail.smtp.port"));		
		mailFrom = bundle.getString("mailFrom");
		username = bundle.getString("username");
		password = bundle.getString("password");
    }
    static public void send(String text,String subject,String mailTo) {
        Session session = Session.getInstance(
                props,
                new Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                }
        );
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(mailFrom));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mailTo));
            message.setSubject(subject);
            message.setText(text);
            Transport.send(message);
        } catch (MessagingException e) {
           e.printStackTrace();
        }
    }


}
