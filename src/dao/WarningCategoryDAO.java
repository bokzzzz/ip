package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import connection.ConnectionPool;
import dto.WarningCategoryDTO;

public class WarningCategoryDAO {
	public List<WarningCategoryDTO> getAllCategory(){
		List<WarningCategoryDTO> categories  = new LinkedList<WarningCategoryDTO>();
		
		String sql = "select * from warningcategory";
		
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			
			ResultSet resultSet = statement.executeQuery();
			while(resultSet.next()) {
				WarningCategoryDTO categoty=new WarningCategoryDTO();
				categoty.setId(resultSet.getInt("id"));
				categoty.setName(resultSet.getString("name"));
				categories.add(categoty);
			}
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		
		return categories;
	}
	public String getNameById(int id) {
		String result=null;
		String sql = "select name from warningcategory where id=?";
		
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, id);
			ResultSet resultSet = statement.executeQuery();
			if(resultSet.next()) {
				result=resultSet.getString("name");

			}
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		
		return result;
	}
}
