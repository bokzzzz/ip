package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import connection.ConnectionPool;
import dto.CommentDTO;
import dto.UserDTO;

public class CommentDAO {
	public List<CommentDTO> getAllCommentByPostId(int postId){
		List<CommentDTO> comments=new LinkedList<CommentDTO>();
		String sql="select * from comment  where userpost_id = ? order by datetime asc";
		Connection connection=null;
		try {
			SimpleDateFormat readingFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			SimpleDateFormat outputFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
			connection=ConnectionPool.getConnectionPool().checkOut();
			
			PreparedStatement statement=connection.prepareStatement(sql);
			
			statement.setInt(1, postId);
			
			ResultSet resultSet=statement.executeQuery();
			while(resultSet.next()) {
				CommentDTO comment=new CommentDTO();
				comment.setContent(resultSet.getString("content"));
				comment.setType(resultSet.getString("type"));
				comment.setUserId(resultSet.getInt("user_id"));
				Date date = readingFormat.parse(resultSet.getString("datetime"));
				comment.setDatetime(outputFormat.format(date));
				comment.setPostId(postId);
				comments.add(comment);
			}
			resultSet.close();
		} catch (Exception e) {
			
		}finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		return comments;
	}
	public List<UserDTO> getAllUsersFromCommentsById(int idPost) {
		List<UserDTO> users=new LinkedList<UserDTO>();
		String sql = "SELECT distinct user_id FROM comment where userpost_id=?";
		Connection connection = null;
		try {
			
			connection=ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statment=connection.prepareStatement(sql);
			statment.setInt(1, idPost);
			ResultSet resultSet=statment.executeQuery();
			UserDAO userDAO=new UserDAO();
			while(resultSet.next()) {
				int idUser=resultSet.getInt("user_id");
				users.add(userDAO.getUserById(idUser));
			}
			resultSet.close();
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		return users;
	}
	public boolean addComment(CommentDTO comment) {
		String sql = "insert into comment  values(?, ?, now() , ?, ?)";
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, comment.getUserId());
			statement.setInt(2, comment.getPostId());
			statement.setString(3, comment.getContent());
			statement.setString(4, comment.getType());
			
			
			statement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		return true;
	}
}
