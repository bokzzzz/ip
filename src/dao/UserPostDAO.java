package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import connection.ConnectionPool;
import dto.UserDTO;
import dto.UserPostDTO;

public class UserPostDAO {
	public boolean addUserPost(UserPostDTO post) {
		String sql = "insert into userpost  values(null, ?, ?, ?,now(),?)";
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			
			statement.setString(1, post.getText());
			statement.setString(2, post.getLink());
			statement.setString(3, post.getYoutube());
			statement.setInt(4,post.getUserId());
			
			statement.executeUpdate();
			
			ResultSet resultSet = statement.getGeneratedKeys();
			
			if(resultSet.next())
				post.setId(resultSet.getInt(1));
			resultSet.close();

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		return true;

	}
	public boolean addImagesToUserPost(int idPost,String content) {
		String sql = "insert into imageonpost  values(null, ?, ?)";
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			
			statement.setString(1, content);
			statement.setInt(2, idPost);
			
			statement.executeUpdate();
			
			

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		return true;
	}
	public boolean addVideoToUserPost(int idPost,String content) {
		String sql = "insert into videoonpost  values(null, ?, ?)";
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			
			statement.setString(1, content);
			statement.setInt(2, idPost);
			
			statement.executeUpdate();
			
			

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		return true;
	}
	public List<UserPostDTO> getAllUserPost(){
		List<UserPostDTO> posts  = new LinkedList<UserPostDTO>();
		
		String sql = "SELECT p.* FROM userpost p inner join user u on u.id=p.user_id where status='Active' order by datetime desc;";
		
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			
			ResultSet resultSet = statement.executeQuery();
			 SimpleDateFormat readingFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			 SimpleDateFormat outputFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
			while(resultSet.next()) {
				UserPostDTO post=new UserPostDTO();
				post.setId(resultSet.getInt("id"));
				post.setLink(resultSet.getString("link"));
				post.setYoutube(resultSet.getString("youtube"));
				post.setUserId(resultSet.getInt("user_id"));
				post.setImages(getImagesOnPostById(post.getId()));
				post.setVideo(getVideosOnPostById(post.getId()));
				post.setText(resultSet.getString("text"));
				Date date = readingFormat.parse(resultSet.getString("datetime"));
				post.setDatetime(outputFormat.format(date));
				posts.add(post);
			}
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		
		return posts;
	}
	public String[] getImagesOnPostById(int idPost) {
		List<String> images=new LinkedList<String>();
		String sql = "select * from imageonpost where userpost_id= ?";
		Connection connection = null;
		try {
			connection=ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statment=connection.prepareStatement(sql);
			statment.setInt(1, idPost);
			
			ResultSet resultSet=statment.executeQuery();
			while(resultSet.next()) {
				images.add(resultSet.getString("content"));
			}
		resultSet.close();
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		return images.toArray(String[]::new);
	}
	public String[] getVideosOnPostById(int idPost) {
		List<String> images=new LinkedList<String>();
		String sql = "select * from videoonpost where userpost_id= ?";
		Connection connection = null;
		try {
			connection=ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statment=connection.prepareStatement(sql);
			statment.setInt(1, idPost);
			
			ResultSet resultSet=statment.executeQuery();
			while(resultSet.next()) {
				images.add(resultSet.getString("content"));
			}
			resultSet.close();
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		return images.toArray(String[]::new);
	}
	public List<UserDTO> getAllUsersFromPost() {
		List<UserDTO> users=new LinkedList<UserDTO>();
		String sql = "SELECT distinct user_id FROM ip.userpost";
		Connection connection = null;
		try {
			connection=ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statment=connection.prepareStatement(sql);
			
			ResultSet resultSet=statment.executeQuery();
			UserDAO userDAO=new UserDAO();
			while(resultSet.next()) {
				int idUser=resultSet.getInt("user_id");
				users.add(userDAO.getUserById(idUser));
			}
			resultSet.close();
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		return users;
	}
	public UserPostDTO getUserPostById(int id){
		UserPostDTO post  = null;
		
		String sql = "select * from userpost where id= ?";
		
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, id);
			ResultSet resultSet = statement.executeQuery();
			 SimpleDateFormat readingFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			 SimpleDateFormat outputFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
			 if(resultSet.next()) {
				post=new UserPostDTO();
				post.setId(resultSet.getInt("id"));
				post.setLink(resultSet.getString("link"));
				post.setYoutube(resultSet.getString("youtube"));
				post.setUserId(resultSet.getInt("user_id"));
				post.setImages(getImagesOnPostById(post.getId()));
				post.setVideo(getVideosOnPostById(post.getId()));
				post.setText(resultSet.getString("text"));
				Date date = readingFormat.parse(resultSet.getString("datetime"));
				post.setDatetime(outputFormat.format(date));
			}
			 resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		
		return post;
	}
}
