package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import connection.ConnectionPool;
import dto.UserDTO;

public class UserDAO {
	public List<UserDTO> getAllUsers() {
		List<UserDTO> users  = new LinkedList<UserDTO>();
		
		String sql = "select * from user";
		
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			
			ResultSet resultSet = statement.executeQuery();
			while(resultSet.next()) {
				UserDTO user=new UserDTO();
				user.setId(resultSet.getInt("id"));
				user.setName(resultSet.getString("name"));
				user.setSurname(resultSet.getString("surname"));
				user.setUsername(resultSet.getString("username"));
				user.setEmail(resultSet.getString("email"));
				user.setPassword(resultSet.getString("password"));
				user.setCountry(resultSet.getString("country"));
				user.setRegion(resultSet.getString("region"));
				user.setCity(resultSet.getString("city"));
				user.setRole(resultSet.getString("role"));
				user.setStatus(resultSet.getString("status"));
				user.setNotification(resultSet.getString("notification"));
				user.setImage(resultSet.getString("image"));
				user.setNumberOfLogins(resultSet.getInt("NumberOfLogins"));
				user.setIsLoggedIn(resultSet.getInt("isLoggedIn"));
				users.add(user);
			}
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		
		return users;
	}
	public UserDTO getUserByUsernameAndPassword(String username,String password) {
		String sql = "select * from user where username=? and password=?";
		UserDTO user = null;
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, username);
			statement.setString(2, password);
			ResultSet resultSet = statement.executeQuery();
			if(resultSet.next()) {
				user=new UserDTO();
				user.setId(resultSet.getInt("id"));
				user.setName(resultSet.getString("name"));
				user.setSurname(resultSet.getString("surname"));
				user.setUsername(resultSet.getString("username"));
				user.setEmail(resultSet.getString("email"));
				user.setPassword(resultSet.getString("password"));
				user.setCountry(resultSet.getString("country"));
				user.setRegion(resultSet.getString("region"));
				user.setCity(resultSet.getString("city"));
				user.setRole(resultSet.getString("role"));
				user.setImage(resultSet.getString("image"));
				user.setStatus(resultSet.getString("status"));
				user.setNumberOfLogins(resultSet.getInt("NumberOfLogins"));
				user.setIsLoggedIn(resultSet.getInt("isLoggedIn"));
				user.setNotification(resultSet.getString("notification"));
			}
			resultSet.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		
		return user;
	}
	public boolean isUsernameExist(String username) {
		String sql = "select count(*) as numberOfUsername from user where username=?";
		boolean result=false;
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, username);
			ResultSet resultSet = statement.executeQuery();
			if(resultSet.next()) {
				int res=resultSet.getInt("numberOfUsername");
				if(res>0)
					result=true;
			}
			resultSet.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		
		return result;
	}
	public boolean isEmailExist(String email) {
		String sql = "select count(*) as numberOfEmail from user where email=?";
		boolean result=false;
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, email);
			ResultSet resultSet = statement.executeQuery();
			if(resultSet.next()) {
				int res=resultSet.getInt("numberOfEmail");
				if(res>0)
					result=true;
			}
			resultSet.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		
		return result;
	}
	public boolean addUser(UserDTO user) {
		String sql = "insert into user values(null, ?, ?, ?, ?, ?, null, null, null, 'Waiting', 'Classic', null,null, 0, 0)";
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			
			statement.setString(1, user.getUsername());
			statement.setString(2, user.getName());
			statement.setString(3, user.getSurname());
			statement.setString(4, user.getPassword());
			statement.setString(5, user.getEmail());
			
			statement.executeUpdate();
			
			ResultSet resultSet = statement.getGeneratedKeys();
			
			if(resultSet.next())
				user.setId(resultSet.getInt(1));
			resultSet.close();

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		return true;

	}
	public boolean updateInfo(UserDTO user) {
		String sql = "update user set image= ? , country= ? , notification= ? , region= ? , city= ? where username= ?";
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			
			statement.setString(1, user.getImage());
			statement.setString(2, user.getCountry());
			statement.setString(3, user.getNotification());
			statement.setString(4, user.getRegion());
			statement.setString(5, user.getCity());
			statement.setString(6, user.getUsername());
			
			statement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		return true;
	}
	public boolean updateLoggedInStatus(UserDTO user) {
		String sql = "update user set isLoggedIn= ?  where username= ?";
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			
			statement.setInt(1, user.getIsLoggedIn());
			statement.setString(2, user.getUsername());
			
			statement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		return true;
	}
	public List<UserDTO> getAllUsersWithEmailNotification(int idUser) {
		List<UserDTO> users  = new LinkedList<UserDTO>();
		
		String sql = "SELECT name,surname,email FROM user where role='classic' and status='active' and notification like '%email%' and id != ?";
		
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			
			statement.setInt(1,idUser );
			ResultSet resultSet = statement.executeQuery();
			while(resultSet.next()) {
				UserDTO user=new UserDTO();
				user.setName(resultSet.getString("name"));
				user.setSurname(resultSet.getString("surname"));
				user.setEmail(resultSet.getString("email"));
				users.add(user);
			}
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		
		return users;
	}
	public UserDTO getUserById(int id) {
		String sql = "select * from user where id=?";
		UserDTO user = null;
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, id);
			ResultSet resultSet = statement.executeQuery();
			if(resultSet.next()) {
				user=new UserDTO();
				user.setId(resultSet.getInt("id"));
				user.setName(resultSet.getString("name"));
				user.setSurname(resultSet.getString("surname"));
				user.setUsername(resultSet.getString("username"));
				user.setEmail(resultSet.getString("email"));
				user.setPassword(resultSet.getString("password"));
				user.setCountry(resultSet.getString("country"));
				user.setRegion(resultSet.getString("region"));
				user.setCity(resultSet.getString("city"));
				user.setRole(resultSet.getString("role"));
				user.setImage(resultSet.getString("image"));
				user.setStatus(resultSet.getString("status"));
				user.setNumberOfLogins(resultSet.getInt("NumberOfLogins"));
				user.setIsLoggedIn(resultSet.getInt("isLoggedIn"));
				user.setNotification(resultSet.getString("notification"));
			}
			resultSet.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		
		return user;
	}
	public boolean updateNumberOfLogins(UserDTO user) {
		String sql = "update user set NumberOfLogins= NumberOfLogins + 1   where username= ?";
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, user.getUsername());
			
			statement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		return true;
	}
}
