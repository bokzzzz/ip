package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;


import connection.ConnectionPool;
import dto.UserDTO;

public class UserLoginDAO {
	public boolean addUserLoginTime(UserDTO user) {
		String sql = "insert ignore into userlogin values(date(now()),hour(now()),?);";
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			
			statement.setInt(1, user.getId());
			
			statement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		return true;

	}
}
