package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import connection.ConnectionPool;
import dto.WarningPostDTO;

public class WarningPostDAO {
	public boolean addWarningPost(WarningPostDTO post) {
		String sql = "insert into warningpost values(null, ?, ?, ?, ?, ?)";
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			
			statement.setInt(1, post.getUserId() );
			statement.setString(2, post.getTitle());
			statement.setInt(3, post.getUrgent());
			statement.setString(4,	post.getLat());
			statement.setString(5, post.getLng());
			
			statement.executeUpdate();
			ResultSet resultSet = statement.getGeneratedKeys();
			
			if(resultSet.next())
				post.setId(resultSet.getInt(1));
			resultSet.close();

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		return true;

	}
	public boolean addCategoryToWarningPost(int postId,int categotyId) {
		String sql = "insert into categoryonpost values(?, ?)";
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			
			statement.setInt(1, categotyId );
			statement.setInt(2, postId);
			
			statement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		return true;

	}
	public List<WarningPostDTO> getAllWarningPost() {
		List<WarningPostDTO> posts  = new LinkedList<WarningPostDTO>();
		
		String sql = "Select title,id from warningpost";
		
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			
			ResultSet resultSet = statement.executeQuery();
			while(resultSet.next()) {
				WarningPostDTO post=new WarningPostDTO();
				post.setTitle(resultSet.getString("title"));
				post.setId(resultSet.getInt("id"));
				posts.add(post);
			}
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		
		return posts;
	}
	public List<WarningPostDTO> getAllUrgentWarningPost() {
		List<WarningPostDTO> posts  = new LinkedList<WarningPostDTO>();
		
		String sql = "Select title,id from warningpost where urgent=1";
		
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			
			ResultSet resultSet = statement.executeQuery();
			while(resultSet.next()) {
				WarningPostDTO post=new WarningPostDTO();
				post.setTitle(resultSet.getString("title"));
				post.setId(resultSet.getInt("id"));
				posts.add(post);
			}
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		
		return posts;
	}
	public WarningPostDTO getWarningPostById(Integer id) {
		WarningPostDTO post  = null;
		
		String sql = "Select * from warningpost where id=?";
		
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1,id);
			
			ResultSet resultSet = statement.executeQuery();
			if(resultSet.next()) {
				post=new WarningPostDTO();
				post.setTitle(resultSet.getString("title"));
				post.setId(resultSet.getInt("id"));
				post.setLat(resultSet.getString("lat"));
				post.setLng(resultSet.getString("long"));
				post.setUserId(resultSet.getInt("user_id"));
				post.setCategoriesId(getCategoriesByPostId(post.getId()));
			}
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		
		return post;
	}
	public List<Integer> getCategoriesByPostId(int idPost){
		List<Integer> ids  = new LinkedList<Integer>();
		
		String sql = "SELECT warningcategory_id FROM categoryonpost where warningpost_id=?";
		
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1,idPost);
			
			ResultSet resultSet = statement.executeQuery();
			while(resultSet.next()) {
				ids.add(resultSet.getInt("warningcategory_id"));
			}
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		
		return ids;
	}
}
