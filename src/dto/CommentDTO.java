package dto;

public class CommentDTO {
	private String datetime;
	private int userId;
	public int getUserId() {
		return userId;
	}
	public CommentDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getPostId() {
		return postId;
	}
	public void setPostId(int postId) {
		this.postId = postId;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDatetime() {
		return datetime;
	}
	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}
	private int postId;
	private String content;
	private String type;
}
